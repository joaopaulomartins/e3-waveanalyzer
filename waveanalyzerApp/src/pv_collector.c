#include <aSubRecord.h>
#include <registryFunction.h>
#include <epicsExport.h>

#include <string.h> // Provides memcpy prototype
#include <stdlib.h> // Provides calloc prototype
#include <stdint.h>
#include <stdio.h>

#define MAXPOINTS 200000

/*
 *   Simply copy the input waveform record VAL
 *   into the output waveform
*/
static int waveform_copy(aSubRecord *precord) {
    unsigned long numPoints;
    double *input_wave;
  
    // Find the number of elements of the input
    numPoints = precord->nea;
    if (numPoints > MAXPOINTS) {
        printf("[aSub ERROR] Number of elements of the waveform is too high\n");
        return -1;
    }

    /* Point to waveform values */
    input_wave = (double *) precord->a;

    /* Copy it to the output PV*/
    memcpy(precord->vala, input_wave, numPoints * sizeof(double));

    /* Done! */
    return 0;
}
 
epicsRegisterFunction(waveform_copy);
